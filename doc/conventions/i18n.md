**11. Mehrsprachigkeit**

11.1. Mehrsprachigkeit erweitern in allen Modulen in denen bereits Sprachdateien verwendet werden und in neuen Modulen. Siehe auch [Übersetzungen im Masterportal](../languages_de.md).

11.2. Es wird die Bibliothek [i18next]( (https://www.i18next.com/)) verwendet.

11.3. Es sind mindestens die Sprachdateien in Englisch und Deutsch zu pflegen, weitere Sprachen sind optional.

11.4. console.warns und console.errors sind auf Englisch zu schreiben, ebenso die jsdoc.

11.5. Die Fallback-Sprache ist Deutsch.

11.6. Die Dokumentation ist auf Deutsch und Englisch in den jeweiligen Dateien zu schreiben (z.B. languages_de.md und languages_en.md).